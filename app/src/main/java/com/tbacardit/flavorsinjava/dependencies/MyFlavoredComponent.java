package com.tbacardit.flavorsinjava.dependencies;

import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Component(modules = {AndroidInjectionModule.class, MyFlavoredModule.class})
public interface MyFlavoredComponent {
	void inject(DependencyInjectionFragment dependencyInjectionFragment);
}
