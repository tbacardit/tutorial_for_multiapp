package com.tbacardit.flavorsinjava.dependencies;

import com.tbacardit.flavorsinjava.usecase.VeryComplexMathUseCase;

import dagger.Module;
import dagger.Provides;

//Gets dependencies
@Module
public class MyFlavoredModule {
	@Provides
	static VeryComplexMathUseCase providesMath() {
		return new MathImpl();
	}

}
