package com.tbacardit.flavorsinjava.dependencies;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.tbacardit.flavorsinjava.R;
import com.tbacardit.flavorsinjava.heritages.HeritageMathImpl;
import com.tbacardit.flavorsinjava.usecase.VeryComplexMathUseCase;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

public class DependencyInjectionFragment extends Fragment implements HasActivityInjector {
	@BindView(R.id.solution_text)
	TextView solution;
	@BindView(R.id.solve_button)
	Button solveButton;

	@Inject
	DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

	@Inject
	VeryComplexMathUseCase useCase;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.activity_math, container, false);;
		ButterKnife.bind(this, view);
		DaggerMyFlavoredComponent.create().inject(this);
		return view;
	}

	@OnClick(R.id.solve_button)
	public void solveAction(View view){
		new Thread(()->{
			try {
				Double result = (double) useCase.solveMath(1, 2);
				this.getActivity().runOnUiThread(()->{
					solution.setText(String.format(getString(R.string.solution_text), result));
				});
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}).start();
	}
	@OnClick(R.id.solve_with_simple)
	void onClickSimpleMath(View v){
		new Thread(()->{
			try {
				Double result = useCase.complexEquationSolver(1,2);
				this.getActivity().runOnUiThread(()->{
					solution.setText(String.format(getString(R.string.solution_text), result));
				});
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}).start();
	}

	@Override
	public AndroidInjector<Activity> activityInjector() {
		return dispatchingAndroidInjector;
	}
}
