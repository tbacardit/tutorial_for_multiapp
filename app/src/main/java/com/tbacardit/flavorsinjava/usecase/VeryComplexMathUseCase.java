package com.tbacardit.flavorsinjava.usecase;

public interface VeryComplexMathUseCase {
	int solveMath(int number, int number2) throws InterruptedException;
	double complexEquationSolver(double number, double number2) throws InterruptedException;
}
