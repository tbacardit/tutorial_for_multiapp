package com.tbacardit.flavorsinjava.services;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.tbacardit.flavorsinjava.BuildConfig;
import com.tbacardit.flavorsinjava.R;

import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ServiceFragment extends Fragment {
	@BindView(R.id.solve_button)
	Button solve;
	@BindView(R.id.solution_text)
	protected TextView mathQuestion;
	private boolean permission = true;
	private IMyAidlInterface addService;
	private static int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE = 5469;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.activity_service_math, container, false);
		ButterKnife.bind(this, view);
		solve.setEnabled(false);
		checkPermission();
		initConnection();
		return view;
	}

	private void initConnection() {
		if (addService == null) {
			Intent i = new Intent("MathService").setPackage(BuildConfig.APPLICATION_ID);
			boolean ret = this.getContext().bindService(i, serviceConnection, Context.BIND_AUTO_CREATE);
			Log.d("TAG", "initService() bound with " + ret);
			//Boolean bol = this.getContext().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
		}
	}

	private ServiceConnection serviceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
			Log.d("", "Service Connected");
			try {
				addService = IMyAidlInterface.Stub.asInterface(iBinder);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				solve.setEnabled(true);
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			Log.d("", "Service Disconnected");
			addService = null;
		}
	};


	@TargetApi(Build.VERSION_CODES.M)
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE) {
			if (!Settings.canDrawOverlays(this.getContext())) {
				// You don't have permission
				checkPermission();
			} else {
				// Do as per your logic
			}

		}

	}

	private void checkPermission() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			permission = Settings.canDrawOverlays(this.getContext());
			if (!permission) {
				Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
						Uri.parse("package:" + this.getContext().getPackageName()));
				startActivityForResult(intent, ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE);
			}
		}
	}
	@OnClick(R.id.solve_with_alert)
	void onClickAlert(View v){
		new Thread(()->{
			int result = -1;
			try {
				result = addService.solveAndShowMe(3.0, 4.0);
			} catch (RemoteException e) {
				e.printStackTrace();
			} finally {
				if(result==-1){
					this.getActivity().runOnUiThread(()->{
						mathQuestion.setText(R.string.no_alert);
					});
				}
			}
		}).start();
	}
	@OnClick(R.id.solve_button)
	void onClickMath(View v){
		new Thread(()->{
			String result = "No result";
			try {
				result= String.valueOf(addService.solveMathComplex(1,2));
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			String finalResult = result;
			this.getActivity().runOnUiThread(()->{
				mathQuestion.setText(finalResult);
			});
		}).start();
	}
	@OnClick(R.id.solve_with_simple)
	void onClickSimpleMath(View v){
		new Thread(()->{
			String result = "No result";
			try {
				result= String.valueOf(addService.solveMath(1,2));
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			String finalResult = result;
			this.getActivity().runOnUiThread(()->{
				mathQuestion.setText(finalResult);
			});
		}).start();
	}
}
