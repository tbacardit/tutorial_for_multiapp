package com.tbacardit.flavorsinjava.heritages;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.os.Bundle;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tbacardit.flavorsinjava.R;

public abstract class HeritageFragment extends Fragment {
	@BindView(R.id.solution_text)
	protected TextView mathQuestion;
	protected HeritageMath math;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                             Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.activity_math, container, false);;
		ButterKnife.bind(this, view);
		math=new HeritageMathImpl();
		return view;
	}

	@OnClick(R.id.solve_button)
	public void onClickMath(View v){
		new Thread(()->{
			String result = "No result";
			try {
				result= String.valueOf(math.solveMath(1,2));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String finalResult = result;
			this.getActivity().runOnUiThread(()->{
				mathQuestion.setText(finalResult);
			});
		}).start();
	}
	@OnClick(R.id.solve_with_simple)
	void onClickSimpleMath(View v){
		new Thread(()->{
			String result = "No result";
			try {
				result= String.valueOf(math.complexEquationSolver(1,2));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String finalResult = result;
			this.getActivity().runOnUiThread(()->{
				mathQuestion.setText(finalResult);
			});
		}).start();
	}

}
