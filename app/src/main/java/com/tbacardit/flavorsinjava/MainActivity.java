package com.tbacardit.flavorsinjava;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.tbacardit.flavorsinjava.dependencies.DependencyInjectionFragment;
import com.tbacardit.flavorsinjava.heritages.HeritageFragmentImpl;
import com.tbacardit.flavorsinjava.services.ServiceFragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
	@BindView(R.id.depedencies)
	FrameLayout depedencies;
	@BindView(R.id.heritage)
	FrameLayout heritage;
	@BindView(R.id.services)
	FrameLayout services;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
		FragmentManager fragMgr = getSupportFragmentManager();
		FragmentTransaction fragTrans = fragMgr.beginTransaction();

		HeritageFragmentImpl heritageFragment = new HeritageFragmentImpl();
		DependencyInjectionFragment DIFragment = new DependencyInjectionFragment();
		ServiceFragment serviceFragment = new ServiceFragment();
		fragTrans.replace(R.id.heritage, heritageFragment);
		fragTrans.replace(R.id.depedencies, DIFragment);
		fragTrans.replace(R.id.services, serviceFragment);
		fragTrans.commit();

	}

}
