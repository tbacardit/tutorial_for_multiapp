// IMyAidlInterface.aidl
package com.tbacardit.flavorsinjava.services;

// Declare any non-default types here with import statements

interface IMyAidlInterface {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    int solveMath(int number, int number2);
    double solveMathComplex(double number, double number2);
    int solveAndShowMe(double number, double number2);
}
