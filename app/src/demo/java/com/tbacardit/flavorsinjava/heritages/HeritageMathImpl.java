package com.tbacardit.flavorsinjava.heritages;

public class HeritageMathImpl extends HeritageMath {
	@Override
	public double complexEquationSolver(double number, double number2) throws InterruptedException {
		return Math.pow(number, number2);
	}

	@Override
	public int solveMath(int number, int number2) throws InterruptedException {
		return number+number2;
	}
}
