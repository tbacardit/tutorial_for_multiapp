package com.tbacardit.flavorsinjava.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;

import androidx.annotation.Nullable;

public class SuperCoolService extends Service {
	private Context getContext(){
		return this;
	}
	private final IMyAidlInterface.Stub binder = new IMyAidlInterface.Stub() {
		@Override
		public int solveMath(int number, int number2) throws RemoteException {
			return number-number2;
		}

		@Override
		public double solveMathComplex(double number, double number2) throws RemoteException {
			return number/number2;
		}

		@Override
		public int solveAndShowMe(double number, double number2) throws RemoteException {
			return -1;
		}
	};

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}
	@Override
	public void onCreate() {
		super.onCreate();
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		return START_STICKY;
	}
}
