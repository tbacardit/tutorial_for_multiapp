package com.tbacardit.flavorsinjava.heritages;

import com.tbacardit.flavorsinjava.EvenMOREComplexMath;

public class HeritageMathImpl extends HeritageMath {
	@Override
	public double complexEquationSolver(double number, double number2) throws InterruptedException {
		if(new EvenMOREComplexMath().canidomath())
			Thread.sleep(2000);
		return Math.pow(number, number2);
	}

	@Override
	public int solveMath(int number, int number2) throws InterruptedException {
		if(new EvenMOREComplexMath().canidomath())
			Thread.sleep(1000);
		return 3;
	}
}
