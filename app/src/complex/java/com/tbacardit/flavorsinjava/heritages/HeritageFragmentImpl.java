package com.tbacardit.flavorsinjava.heritages;

import android.view.View;

import com.tbacardit.flavorsinjava.R;

import butterknife.OnClick;

public class HeritageFragmentImpl extends HeritageFragment {
	@OnClick(R.id.solve_button)
	public void onClickMath(View v){
		new Thread(()->{
			String result = "No result";
			try {
				result= String.valueOf(math.solveMath(4,5));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String finalResult = result;
			this.getActivity().runOnUiThread(()->{
				mathQuestion.setText(finalResult);
			});
		}).start();
	}
}
