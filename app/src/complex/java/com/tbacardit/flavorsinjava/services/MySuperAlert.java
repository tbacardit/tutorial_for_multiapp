package com.tbacardit.flavorsinjava.services;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.tbacardit.flavorsinjava.R;

import androidx.annotation.NonNull;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MySuperAlert extends Dialog {
	@BindView(R.id.solve_text)
	TextView solveText;
	double number1, number2;
	public MySuperAlert(@NonNull Context context, double number1, double number2) {
		super(context, R.style.DialogTheme);
		this.number1=number1;
		this.number2=number2;

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.alert_math_dialog);
		ButterKnife.bind(this);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
		} else {
			getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		}
		doSetup();
	}
	private void doSetup(){
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.width = 300;
		lp.height = 200;
		lp.dimAmount = 0.0f;
		lp.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
//		lp.y = 50;
//		lp.x = 50;
		getWindow().setAttributes(lp);
//		getWindow().setGravity(Gravity.BOTTOM);
		setCancelable(false);

	}
	public void showAlert() {
		if (!isShowing()) {
			Handler hnd = new Handler(getContext().getMainLooper());
			hnd.post(this::show);
		}
	}

	public void doMeMath(Double number1, double number2){

	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		dismiss();
	}
	@OnClick(R.id.solve_alert)
	public void ClickToSolve(View view){
		solveText.setText(String.format(this.getContext().getString(R.string.resolvingline), number1, number2, number1*number2));
	}
}