package com.tbacardit.flavorsinjava.dependencies;

import com.tbacardit.flavorsinjava.EvenMOREComplexMath;
import com.tbacardit.flavorsinjava.usecase.VeryComplexMathUseCase;

import javax.inject.Inject;

import dagger.Module;

@Module
public class MathImpl implements VeryComplexMathUseCase {
	@Inject
	public MathImpl() {
	}

	@Override
	public double complexEquationSolver(double number, double number2) throws InterruptedException {
		if(new EvenMOREComplexMath().canidomath())
			Thread.sleep(2000);
		return Math.pow(number, number2);
	}

	@Override
	public int solveMath(int number, int number2) throws InterruptedException {
		if(new EvenMOREComplexMath().canidomath())
			Thread.sleep(1000);
		return 3;
	}
}
